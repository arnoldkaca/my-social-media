const postsCollection = require('../db').db().collection("posts")
const followsCollection = require('../db').db().collection("follows")
//get an id as an Object
const ObjectID = require('mongodb').ObjectID
const User = require('./User')
const sanitizeHTML = require('sanitize-html') 

let Post = function(_data, _userId, _requestedPostId) {
    this.errors = []
    this.data = _data
    this.userId = _userId
    this.requestedPostId = _requestedPostId
}

Post.prototype.cleanUp = function() {
    
    // Check if the name of the post and post are strings
    if(typeof(this.data.tittle) != "string") { this.data.tittle = "" }
    if(typeof(this.data.body) != "string") { this.data.body = "" }

    // Get rid of bogus properties
    this.data = {
        title: sanitizeHTML(this.data.title.trim(), {allowedTags: [], allowedAttributes: {}}),
        body: sanitizeHTML(this.data.body.trim(), {allowedTags: [], allowedAttributes: {}}),
        createdDate: new Date(),
        author: ObjectID(this.userId)
    }
}
Post.prototype.validate = function() {
    if(this.data.title == ""){ this.errors.push("You must provide a title.") }
    if(this.data.body == ""){ this.errors.push("You must provide post content.") }
}
Post.prototype.create = function() {
    return new Promise((resolve, reject) => {
        this.cleanUp()
        this.validate()

        if(!this.errors.length){
            // Save post into database
            postsCollection.insertOne(this.data).then((info) => {
                resolve(info.ops[0]._id)
            }).catch(() => {
                this.errors.push("Please try again later.")
                reject(this.errors)
            })
        } else {
            reject(this.errors)
        }
    });
}

Post.prototype.update = function(){
    return new Promise( async (resolve, reject)=> {
        try {
            let post = await Post.findSingleById(this.requestedPostId, this.userId)
            
            if(post.isVisitorOwner) {
                //update the db
                let status = await this.actuallyUpdate()
                resolve(status)
            }else {
                reject()
            }

        } catch (error) {
            reject()
        }
    })
}

Post.prototype.actuallyUpdate = function() {
    return new Promise(async (resolve, reject) => {
        this.cleanUp()
        this.validate()
        if(!this.errors.length){
            await postsCollection.findOneAndUpdate({_id: new ObjectID(this.requestedPostId)}, {
                $set: {
                    title: this.data.title,
                    body: this.data.body
                }
            })
            resolve("success")
        } else {
            resolve("failure")
        }
    })
}


Post.reusablePostQuery = function(uniqueOperations, visitorId) {
    return new Promise(async function(resolve, reject) {
        try {
            //run multiple operations
            //project => allows us to spell out exactly what fields we want resulting object to have
            let aggOperations = uniqueOperations.concat([
                {$lookup: {from: "users", localField: "author", foreignField: "_id", as: "authorDocument"}},
                {$project: {
                    title: 1,
                    body: 1,
                    createdDate: 1,
                    authorId: "$author",
                    author: { $arrayElemAt: ["$authorDocument", 0] }
                }}
            ])
            
            let posts = await postsCollection.aggregate(aggOperations).toArray()

            // Clean up auythor property in each post object
            posts = posts.map(function(post) {
                post.isVisitorOwner = post.authorId.equals(visitorId)
                post.authorId = undefined
                
                post.author = {
                    username:post.author.username,
                    avatar: new User(post.author, true).avatar
                }
                return post
            })
            resolve(posts)
        } catch (error) {
            reject()
        }
    })
}

Post.findSingleById = function(id, visitorId) {
    return new Promise(async function(resolve, reject) {
        if(typeof(id) != "string" || !ObjectID.isValid(id)){
            reject()
            return
        }

        let posts = await Post.reusablePostQuery([
            {$match: {_id: new ObjectID(id)}}
        ], visitorId)

        if(posts.length){
            console.log(posts[0])
            resolve(posts[0])
        }else {
            reject()
        }
    })
}

Post.findByAuthorId = function(authorId){

    // 1 for ascending 
    // -1 for descending
    return Post.reusablePostQuery([
        {$match: {author: authorId}},
        {$sort: {createdDate: -1}}
    ])
}

Post.delete = function (postIdToDelete, currentUserId) {
    return new Promise(async (resolve, reject) => {
        try {
            let post = await Post.findSingleById(postIdToDelete, currentUserId)
            if(post.isVisitorOwner) {
                await postsCollection.deleteOne({_id: new ObjectID(postIdToDelete)})
                resolve()
            } else {
                // delete a post they do not own
                reject()
            }
        } catch (error) {
            // post id not valid 
            // post does not exist
            reject()
        }
    })
}


Post.search = function (searchTerm) {
    return new Promise( async (resolve, reject) => {
        try {
            if (typeof(searchTerm) == "string") {
                let posts = await Post.reusablePostQuery([
                    {$match: {$text: {$search: searchTerm}}},
                    {$sort: {score: {$meta: "textScore"}}}
                ])
                resolve(posts)
            } else {
                reject()
            }
        } catch (error) {
            reject()
        }
    })
}

Post.countPostsByAuthor = function(id) {
    return new Promise( async (resolve, reject) => {
        let postCount = await postsCollection.countDocuments({author: id})
        resolve(postCount)
    })
}

Post.getFeed = async function(id) {
    // create and array of the user ids that the current user followes
    let followedUsers = await followsCollection.find({authorId: new ObjectID(id)}).toArray()
    followedUsers = followedUsers.map(user => {
        return user.followedId
    })

    // look for posts where the author is in the above array of followed users
    return Post.reusablePostQuery([
        {$match: {author: {$in: followedUsers}}},
        {$sort: {createdDate: -1}}
    ])

}


module.exports = Post