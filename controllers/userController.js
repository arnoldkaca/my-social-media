const User = require('../models/User')
const Post = require('../models/Post')
const Follow = require('../models/Follow')
const jwt = require('jsonwebtoken')


exports.doesUsernameExist = function(req, res) {
    User.findByUsername(req.body.username).then( () => {
        res.json(true)
    }).catch(() => {
        res.json(false)
    })
}

exports.doesEmailExist = async function(req, res) {
    let emailBool = await User.doesEmailExist(req.body.email)
    res.json(emailBool)
}


exports.sharedProfileData = async function (req, res, next) {
    let isVisitorsProfile = false
    let isFollowing = false

    if(req.session.user) {
        isVisitorsProfile = req.profileUser._id.equals(req.session.user._id)
        isFollowing = await Follow.isVisitorFollowing(req.profileUser._id, req.visitorId)
    } else {

    }

    req.isVisitorsProfile = isVisitorsProfile
    req.isFollowing = isFollowing

    // retetrieve counts for post, follower, following

    let postCountPromise = Post.countPostsByAuthor(req.profileUser._id)
    let followerCountPromise = Follow.countFollowersById(req.profileUser._id)
    let followingCountPromise = Follow.countFollowingsById(req.profileUser._id)

    //aray destructuring
    let [postCount, followerCount, followingCount] = await Promise.all([postCountPromise, followerCountPromise, followingCountPromise])
    
    req.postCount = postCount
    req.followerCount = followerCount
    req.followingCount = followingCount
    

    next()
}

exports.mustBeLoggedIn = function (req, res, next) {
    if(req.session.user) {
        next();
    }else {
        req.flash("errors", "You must be logged in to perform that action");
        req.session.save(function() {
            res.redirect('/');
        })
    }
}
exports.apiMustBeLoggedIn = function (req, res, next) {
    try {
        req.apiUser = jwt.verify(req.body.token, process.env.JWTSECRET)
        next()
    } catch {
        res.json({"error" : "You must provide a valid token"})
    }
}

exports.apiGetPostsByUsername = async function(req, res) {
    try {
        let authorDoc = await User.findByUsername(req.params.username)
        let posts = await Post.findByAuthorId(authorDoc._id)
        res.json(posts)
    } catch (error) {
        res.json({"error" : "Invalid User requested"})
    }
}

/* TRADITIONAL APPROACH */
// exports.login = (req, res) => {
//     let user = new User(req.body)
//     user.login(function(result){
//         res.send(result)
//     })
// }

/* PROMISE APPROACH */
exports.login = (req, res) => {
    let user = new User(req.body)
    user.login().then(function(result){
        req.session.user = {avatar: user.avatar, username: user.data.username, _id: user.data._id}
        //update the session data in the database
        req.session.save(() => {
            res.redirect('/')
        })
    }).catch(function(err){
        req.flash('errors', err)
        req.session.save(function(){
            res.redirect('/')
        })
    })
}
exports.apiLogin = (req, res) => {
    let user = new User(req.body)
    user.login().then(function(result){
        // second result is the salt
        res.json(jwt.sign({ _id: user.data._id }, process.env.JWTSECRET,{expiresIn: '10d'}))
    }).catch(function(err){
        res.json({})
    })
}
exports.logout = (req, res) => {
    req.session.destroy(()=> {
        res.redirect('/')
    })
}
exports.register = function(req, res) {
    let user = new User(req.body)
    user.register().then(() => {
        req.session.user = {username: user.data.username, avatar: user.avatar, _id: user.data._id}
        req.session.save(function() {
            res.redirect('/')
        })
    }).catch((regErrors) => {
        regErrors.forEach(function(error) {
            req.flash('regErrors', error)
        })
        req.session.save(function() {
            res.redirect('/')
        })
    })
}
exports.home = async (req, res) => {
    if(req.session.user){
        //get posts feed for current user
        let posts = await Post.getFeed(req.session.user._id)
        console.log(posts)
        res.render('home-dashboard', {posts: posts})
    } else {
        res.render('home-guest', {regErrors: req.flash('regErrors')})
    }
}

exports.ifUserExists = function(req, res, next) {
    User.findByUsername(req.params.username).then(function(userDocument){
        req.profileUser = userDocument
        next()
    }).catch(function() {
        res.render('404')
    })
}

exports.profilePostsScreen = function(req, res){

    //ask our post model for posts by a certain author id
    console.log(req.followerCount)
    Post.findByAuthorId(req.profileUser._id ).then(function(posts){
        res.render('profile', {
            title: `${req.profileUser.username}`,
            currentPage: "posts",
            posts: posts,
            profileUsername: req.profileUser.username,
            profileAvatar: req.profileUser.avatar,
            isFollowing: req.isFollowing,
            isVisitorsProfile: req.isVisitorsProfile,
            counts: {
                postCount: req.postCount,
                followerCount: req.followerCount,
                followingCount: req.followingCount
            }
        })
    }).catch(function(){
        res.render('404')
    })    
}

exports.profileFollowersScreen = async function(req, res) {
    try {
        let followers = await Follow.getFollowersById(req.profileUser._id)
        res.render('profile-followers', {
            currentPage: "followers",
            followers: followers,
            profileUsername: req.profileUser.username,
            profileAvatar: req.profileUser.avatar,
            isFollowing: req.isFollowing,
            isVisitorsProfile: req.isVisitorsProfile,
            counts: {
                postCount: req.postCount,
                followerCount: req.followerCount,
                followingCount: req.followingCount
            }
        })
    } catch (e) {
        res.render('404')
    }
}
exports.profileFollowingScreen = async function(req, res) {
    try {
        let following = await Follow.getFollowingById(req.profileUser._id)
        res.render('profile-following', {
            currentPage: "following",
            following: following,
            profileUsername: req.profileUser.username,
            profileAvatar: req.profileUser.avatar,
            isFollowing: req.isFollowing,
            isVisitorsProfile: req.isVisitorsProfile,
            counts: {
                postCount: req.postCount,
                followerCount: req.followerCount,
                followingCount: req.followingCount
            }
        })
    } catch (e) {
        res.render('404')
    }
}