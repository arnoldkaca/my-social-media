const bcrypt = require('bcryptjs')
const usersCollection = require('../db').db().collection('users')
const validator = require('validator')
const md5 = require('md5')

let User = function(data, getAvatar) {
    this.data = data
    this.errors = []

    if(getAvatar == undefined) {getAvatar = false}
    if(getAvatar){
        this.getAvatar()
    }
}

User.prototype.cleanUp = function() {
    if (typeof(this.data.username) != "string") {this.data.username = ""}
    if (typeof(this.data.email) != "string") {this.data.email = ""}
    if (typeof(this.data.password) != "string") {this.data.password = ""}

    //get rid of any bogus properties
    this.data = {
        username: this.data.username.trim().toLowerCase(),
        email: this.data.email.trim().toLowerCase(),
        password: this.data.password
    }
}

User.prototype.validateUser = function() {
    return new Promise(async (resolve, reject) => {
        if (this.data.username == "") {this.errors.push("You must provide a username.")}
        if (this.data.username != "" && !validator.isAlphanumeric(this.data.username)){this.errors.push("Username can contain only letters and numbers.")}
        if (!validator.isEmail(this.data.email)) {this.errors.push("You must provide a email.")}
        if (this.data.password == "") {this.errors.push("You must provide a password.")}
        if (this.data.password.length > 0 && this.data.password.length < 12) {this.errors.push("Password must have at least 12 characters.")}
        if (this.data.password.length > 50) {this.errors.push("Password must not exceed 50 characters.")}
        if (this.data.username.length > 0 && this.data.username.length < 3) {this.errors.push("Username must have at least 3 characters.")}
        if (this.data.username.length > 30) {this.errors.push("Username must not exceed 30 characters.")}
    
        // Only if username is valid then check to see if is already taken
        if(this.data.username.length > 2 && this.data.username.length < 31 && validator.isAlphanumeric(this.data.username)){
            let usernameExists =await usersCollection.findOne({username: this.data.username})
            if(usernameExists){
                this.errors.push("That username is already taken.")
            }
        }
    
        // Only if email is valid then check to see if is already taken
        if(validator.isEmail(this.data.email)){
            let emailExists = await usersCollection.findOne({email: this.data.email})
            if(emailExists){
                this.errors.push("That email is already taken.")
            }
        }
        resolve()
    
    })
}

/* TRADITION APPROACH */
// User.prototype.login = function(callback){
//     this.cleanUp()
//     usersCollection.findOne({username: this.data.username}, (err, attemptedUser) => {
//         if(attemptedUser && attemptedUser.password == this.data.password){
//             callback("Congrats!!!")
//         }else {
//             callback("Invalid Password")
//         }
//     })
// }


/* PROMISE APPROACH */
// https://codepen.io/anon/pen/eabJgY?editors=0010
// async function() {
//     await Promise.all([promise1, promise2, promise3, promise4])
//     // JavaScript will wait until ALL of the promises have completed
//     console.log("All promises completed. Do something interesting now.")
// }
User.prototype.login = function(){
    return new Promise((resolve, reject) => {
        this.cleanUp()
        usersCollection.findOne({username: this.data.username}).then( (attemptedUser)=>{
            if(attemptedUser && bcrypt.compareSync(this.data.password, attemptedUser.password)){
                this.data = attemptedUser
                this.getAvatar()
                resolve("Congrats!!!")
            }else {
                reject("Invalid Password")
            }
        }).catch((error) => {
            reject("Please Try again later...")
        })
    })
}

User.prototype.register = function(){
    return new Promise(async (resolve, reject) => {
        this.cleanUp()
        await this.validateUser()
    
        if (!this.errors.length){
            //hash user password
            let salt = bcrypt.genSaltSync(10)
            this.data.password = bcrypt.hashSync(this.data.password, salt)
    
            // this.data already cleaned up and ready
            await usersCollection.insertOne(this.data)
            this.getAvatar()
            resolve()
        }else {
            reject(this.errors)
        }
    })
}

User.prototype.getAvatar = function() {
    this.avatar = `https://gravatar.com/avatar/${md5(this.data.email)}?s=128`
}


User.findByUsername = function(username) {
    return new Promise(function(resolve, reject){
        if(typeof(username) != "string"){
            reject()
            return
        }

        usersCollection.findOne({username: username}).then(function(userDoc){
            if(userDoc){
                userDoc = new User(userDoc, true)
                userDoc = {
                    _id: userDoc.data._id,
                    username: userDoc.data.username,
                    avatar: userDoc.avatar
                }
                resolve(userDoc)
            } else {
                reject()
            }
        }).catch(function(){
            reject()
        })

    })
}

User.doesEmailExist = function(email) {
    return new Promise(async (resolve, reject) => {
        if(typeof(email) != "string"){
            resolve(false)
            return
        }
        let user = await usersCollection.findOne({email: email})
        if(user){
            resolve(true)
        } else {
            resolve(false)
        }
    })
}

module.exports = User