# My Social Media

A simple fully-featured social media application.
### [https://aksocial.herokuapp.com/](https://aksocial.herokuapp.com/)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
[Node.js](https://nodejs.org/en/)
```

## Installation Process
A step by step series of examples that tell you how to get a development environment up and running.

To install all the dependencies run this command in the terminal
```
npm install
```

To start the server run this command in the terminal
```
npm run watch
```

## Files to be created for the project
Create a .env file in the root directory
> .env
```
CONNECTIONSTRING=mongodb+srv://<username>:<password>@<host>/<AppName>?retryWrites=true&w=majority
PORT=[your_port_number]
JWTSECRET=[your_secret_jwt]
SENDGRIDAPIKEY=[your_sendgrid_api_key]
EMAIL=[your_email_address]
```

Create a .env file in the root directory
add this in the file